//
//  FontCell.swift
//  HomeQuiz
//
//  Created by Hsu Toby on 2022/3/6.
//

import UIKit

class FontCell: UICollectionViewCell {
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = .lightGray
    }
    
    func configure(with attributedText: NSAttributedString?) {
        textLabel.attributedText = attributedText
    }
    
    @IBOutlet weak var textLabel: UILabel!
}
