//
//  ViewController+ViewModel.swift
//  HomeQuiz
//
//  Created by Hsu Toby on 2022/3/6.
//

import Foundation
import PromiseKit

extension ViewController {
    class ViewModel {
        // MARK: Internal

        var itemsCount: Int { items.count }
        @available(*, deprecated, message: "Version 1")
        var displayString: NSAttributedString? { attributedString(at: selectedIndex, text: displayText) }
        var currentFontUrl: URL? { items[selectedIndex].files?.values.first }

        func attributedString(at index: Int, text: String? = nil) -> NSAttributedString? {
            let fontFamily = items[index].family ?? ""
            let string = createHTMLString(with: text ?? fontFamily, fontFamily: fontFamily)
            return try? NSAttributedString(data: Data(string.utf8),
                                           options: [.documentType: NSAttributedString.DocumentType.html],
                                           documentAttributes: nil)
        }

        func fetchFontItems() -> Promise<Void> {
            guard let url = URL(string: "https://www.googleapis.com/webfonts/v1/webfonts?key=\(apiKey)") else {
                return .init(error: Errors.invalidUrl)
            }

            return URLSession.shared.dataTask(.promise, with: url)
                .compactMap {
                    let decoder = JSONDecoder()
                    do {
                        let font = try decoder.decode(GoogleFont.self, from: $0.data)
                        self.items = font.items ?? []
                        return ()
                    } catch {
                        throw error
                    }
                }
        }

        func downloadFont() -> Promise<UIFont> {
            URLSession.shared.dataTask(.promise, with: currentFontUrl!)
                .compactMap {
                    let bytes = $0.data.withUnsafeBytes {
                        $0.baseAddress?.assumingMemoryBound(to: UInt8.self)
                    }
                    guard let cfData = CFDataCreate(kCFAllocatorDefault, bytes, $0.data.count),
                          let provider = CGDataProvider(data: cfData),
                          let cgFont = CGFont(provider),
                          let fontName = cgFont.postScriptName else {
                        throw Errors.providerInitFail
                    }

                    let fontNameString = String(NSString(string: fontName))
                    if let uiFont = UIFont(name: fontNameString, size: 20) {
                        // UIFont already existed
                        return uiFont
                    }

                    var error: Unmanaged<CFError>?
                    if CTFontManagerRegisterGraphicsFont(cgFont, &error) {
                        return UIFont(name: fontNameString, size: 20)
                    } else {
                        throw Errors.registerFontFail
                    }
                }
        }

        @available(*, deprecated, message: "Version 1")
        func updateText(_ text: String?) {
            displayText = text
        }

        func selectFont(at index: Int) {
            selectedIndex = index
        }

        // MARK: Private

        private var items: [FontItem] = []
        private var selectedIndex: Int = 0
        private var displayText: String?
        private let apiKey = "AIzaSyDF0o_xo1h6T5oPs6D0A1leAwACfcGLlmY"

        private func createHTMLString(with text: String? = nil, fontFamily: String, size: Int = 20) -> String {
            """
            <html>
                <head>
                    <link rel="stylesheet"
                              href="https://fonts.googleapis.com/css2?family=\(fontFamily)">
                        <style>
                            .text {
                                font-family: '\(fontFamily)', serif;
                                font-size: \(size)px;
                                text-align: center;
                            }
                        </style>
                </head>
                <body>
                    <div>
                        <p class="text">\(text ?? fontFamily)</p>
                    </div>
                </body>
            </html>
            """
        }
    }
}

extension ViewController {
    private enum Errors: Error {
        case invalidUrl
        case providerInitFail
        case registerFontFail
    }
}
