import UIKit
import XCTest

struct Quiz3 {
    func funcString(_ n: Int) -> Int {
        let num = abs(n)
        guard num > 1 else {
            return 0
        }

        var count = 0
        for i in 1 ... num {
            if String("\(i)").contains(Character("7")) {
                count += 1
            }
        }
        return count
    }

    func funcDP(_ n: Int) -> Int {
        let num = abs(n)
        guard num >= 7 else {
            return 0
        }

        let str = String("\(num)")
        let length = str.count
        let digits = str.compactMap { Int(String($0)) }

        /*
         1 digit: 0 ~ 9
         2 digits: 0 ~ 99
         3 digits: 0 ~ 999

         0~9 is different digit(object), count is 10
         exclude 7, count is 9
         [0...9] = 1 = (10 - 9) = 10^1 - 9^1
         [0...99] = 19 = (10 - 9) * (10 - 9) = 10^2 - 9^2
         so I guess the formula is 10^digitCount - 9^digitCount.
         */
        let range = (0 ..< length).map {
            Int(pow(10.0, Double($0))) - Int(pow(9.0, Double($0)))
        }

        let numberOfDigit = range.count - 1
        let upperBound = Int(ceil(pow(10.0, Double(numberOfDigit))))
        let firstDigit = digits[0]
        let remainder = num % upperBound

        if firstDigit == 7 {
            return firstDigit * range[numberOfDigit] + remainder + 1
        } else if firstDigit > 7 {
            return (firstDigit - 1) * range[numberOfDigit] + upperBound + funcDP(remainder)
        } else {
            return firstDigit * range[numberOfDigit] + funcDP(remainder)
        }
    }
}

class Quiz3TestCase: XCTestCase {
    func testFuncDP_normalCase_isCorrect() {
        // Arrange
        let sut = Quiz3()
        let testCases = [10, 70, 77, 123]

        // Act
        let result = testCases.map { sut.funcDP($0) }

        // Assert
        XCTAssertEqual(result, [1, 8, 15, 21])
    }

    func testFuncDP_edgeCase_isCorrect() {
        // Arrange
        let sut = Quiz3()
        let testCases = [0, 1000, -1000]

        // Act
        let result = testCases.map { sut.funcDP($0) }

        // Assert
        XCTAssertEqual(result, [0, 271, 271])
    }

    func testFuncString_normalCase_isCorrect() {
        // Arrange
        let sut = Quiz3()
        let testCases = [10, 70, 77, 123]

        // Act
        let result = testCases.map { sut.funcString($0) }

        // Assert
        XCTAssertEqual(result, [1, 8, 15, 21])
    }

    func testFuncString_edgeCase_isCorrect() {
        // Arrange
        let sut = Quiz3()
        let testCases = [0, 1000, -1000]

        // Act
        let result = testCases.map { sut.funcString($0) }

        // Assert
        XCTAssertEqual(result, [0, 271, 271])
    }
}

Quiz3TestCase.defaultTestSuite.run()
