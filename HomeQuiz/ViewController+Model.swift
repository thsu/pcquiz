//
//  Model.swift
//  HomeQuiz
//
//  Created by Hsu Toby on 2022/3/7.
//

import Foundation

extension ViewController {
    struct GoogleFont: Codable {
        let items: [FontItem]?
    }

    struct FontItem: Codable {
        let family: String?
        let files: [String: URL]?
    }
}
