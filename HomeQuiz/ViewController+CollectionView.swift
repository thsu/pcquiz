//
//  ViewController+CollectionView.swift
//  HomeQuiz
//
//  Created by Hsu Toby on 2022/3/6.
//

import Foundation
import UIKit

extension ViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        viewModel.itemsCount
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FontCell", for: indexPath) as? FontCell else {
            return UICollectionViewCell()
        }
        
        cell.configure(with: viewModel.attributedString(at: indexPath.item))
        return cell
    }
}

extension ViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        viewModel.selectFont(at: indexPath.item)
        // version 1
        // textLabel.attributedText = viewModel.displayString
        
        // version 2
        viewModel.downloadFont()
            .done {
                self.textLabel.font = $0
            }
            .catch {
                print($0.localizedDescription)
            }
    }
}
