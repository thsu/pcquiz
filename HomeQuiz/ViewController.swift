//
//  ViewController.swift
//  HomeQuiz
//
//  Created by Hsu Toby on 2022/3/6.
//

import Then
import UIKit

class ViewController: UIViewController {
    // MARK: Internal
    let viewModel = ViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()

        viewModel.fetchFontItems()
            .then {
                self.viewModel.downloadFont()
            }
            .done {
                self.textLabel.font = $0
            }
            .catch {
                print($0.localizedDescription)
            }
            .finally {
                self.collectionView.reloadData()
                // version 1
                // self.textLabel.attributedText = self.viewModel.displayString
            }
    }

    @IBAction func inputAction(_ sender: Any) {
        // version 1
        // viewModel.updateText(textField.text)
        // textLabel.attributedText = viewModel.displayString

        // version 2
        textLabel.text = textField.text
    }

    // MARK: Private

    private func setupUI() {
        let layout = UICollectionViewFlowLayout().with {
            $0.scrollDirection = .vertical
            $0.itemSize = .init(width: collectionView.bounds.width - 20, height: 50)
            $0.minimumLineSpacing = 10
            $0.minimumInteritemSpacing = 10
        }

        collectionView.do {
            $0.collectionViewLayout = layout
        }

        textField.do {
            $0.placeholder = "Hello World"
            $0.text = "Hello World"
            // version 1
            // viewModel.updateText($0.text)
            textLabel.text = $0.text
        }
    }
    
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var collectionView: UICollectionView!
}
